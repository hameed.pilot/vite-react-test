import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import { saveAs } from 'file-saver';
// import XLSX from "xlsx/dist/xlsx.full.min.js";
// import XLSX from "xlsx-js-style";
import * as XLSX from 'xlsx-js-style';

function App() {
  const [count, setCount] = useState(0);

  // const xlsx = XlsxPopulate;

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>
      <div className="card">
        <button
          onClick={() => {
            console.log("this is a test");

            // // STEP 1: Create a new workbook
            // const wb = XLSX.utils.book_new();

            // // STEP 2: Create data rows and styles
            // let row = [
            //   { v: "Courier: 24", t: "s", s: { font: { name: "Courier", sz: 24 } } },
            //   { v: "bold & color", t: "s", s: { font: { bold: true, color: { rgb: "FF0000" } } } },
            //   { v: "fill: color", t: "s", s: { fill: { fgColor: { rgb: "E9E9E9" } } } },
            //   { v: "line\nbreak", t: "s", s: { alignment: { wrapText: true } } },
            // ];


            // // STEP 3: Create worksheet with rows; Add worksheet to workbook
            // const ws = XLSX.utils.json_to_sheet([row]);
            // XLSX.utils.book_append_sheet(wb, ws, "readme demo");

            // // STEP 4: Write Excel file to browser
            // XLSX.writeFile(wb, "xlsx-js-style-demo.xlsx");

            //-------------------------------//
            const data =
            [
              { name: "George Washington", birthday: "1732-02-22" },
              { name: "John Adams", birthday: "1735-10-19" },
            ];

            const worksheet = XLSX.utils.json_to_sheet(data);

            console.log(worksheet)
            for (var i in worksheet) {
              if (typeof worksheet[i] != 'object') continue;
              console.log(i);
              console.log(worksheet[i]);

              worksheet[i].s = {
                // styling for all cells
                font: {
                  name: 'arial',
                  bold: true,
                },
                bold: true,
                alignment: {
                  vertical: 'center',
                  horizontal: 'center',
                  wrapText: '1', // any truthy value here
                },
                border: {
                  right: {
                    style: 'thin',
                    color: '000000',
                  },
                  left: {
                    style: 'thin',
                    color: '000000',
                  },
                },
              };
            }

            const workbook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(workbook, worksheet, "Sheetxxx1");
            XLSX.writeFile(workbook, "DataSheet.xlsx");

          }}
        >
          Test Xlsx Populate
        </button>

        <button onClick={() => setCount((count) => count + 1)}>count is {count}</button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">Click on the Vite and React logos to learn more</p>
    </div>
  );
}

export default App;
